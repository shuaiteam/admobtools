//
//  SceneDelegate.m
//  AdMobTools
//
//  Created by ws on 2021/9/7.
//

#import "SceneDelegate.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "AdMobManager.h"

@interface SceneDelegate ()

@end

@implementation SceneDelegate


- (void)scene:(UIScene *)scene willConnectToSession:(UISceneSession *)session options:(UISceneConnectionOptions *)connectionOptions {
    // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
    // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
    // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.windowScene = (UIWindowScene*)scene;
    [self.window makeKeyAndVisible];

    
    self.window.rootViewController = [UIViewController new];
    
    [AdMobManager startGADCompletionHandler:nil];
    
    [AdMobManager shareInstance].bannerId = @"ca-app-pub-3940256099942544/2934735716";
    [AdMobManager shareInstance].interstitialId = @"ca-app-pub-3940256099942544/4411468910";

    NSLog(@"加载banner");
    [[AdMobManager shareInstance] showBannerViewWithOrigin:CGPointMake(100, 100) superView:self.window];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

        NSLog(@"展示插屏");
        [[AdMobManager shareInstance] showInterstitialWithRootViewController:self.window.rootViewController];
    });
    
}


- (void)sceneDidDisconnect:(UIScene *)scene {
    // Called as the scene is being released by the system.
    // This occurs shortly after the scene enters the background, or when its session is discarded.
    // Release any resources associated with this scene that can be re-created the next time the scene connects.
    // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
}


- (void)sceneDidBecomeActive:(UIScene *)scene {
    // Called when the scene has moved from an inactive state to an active state.
    // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
}


- (void)sceneWillResignActive:(UIScene *)scene {
    // Called when the scene will move from an active state to an inactive state.
    // This may occur due to temporary interruptions (ex. an incoming phone call).
}


- (void)sceneWillEnterForeground:(UIScene *)scene {
    // Called as the scene transitions from the background to the foreground.
    // Use this method to undo the changes made on entering the background.
}


- (void)sceneDidEnterBackground:(UIScene *)scene {
    // Called as the scene transitions from the foreground to the background.
    // Use this method to save data, release shared resources, and store enough scene-specific state information
    // to restore the scene back to its current state.
}


@end
