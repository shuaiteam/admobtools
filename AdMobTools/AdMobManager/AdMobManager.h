//
//  AdMobManager.h
//  AdMobTools
//
//  Created by ws on 2021/9/7.
//

#import <Foundation/Foundation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

//#import "AdMobBannerManager.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AdBannerViewDelegate <NSObject>
@optional
- (void)bannerViewDidReceiveAd:(GADBannerView *)bannerView;
- (void)bannerView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(NSError *)error;
- (void)bannerViewDidRecordImpression:(GADBannerView *)bannerView;
- (void)bannerViewWillPresentScreen:(GADBannerView *)bannerView;
- (void)bannerViewWillDismissScreen:(GADBannerView *)bannerView;
- (void)bannerViewDidDismissScreen:(GADBannerView *)bannerView;
@end


@protocol AdFullScreenDelegate <NSObject>
@optional
- (void)ad:(nonnull id<GADFullScreenPresentingAd>)ad didFailToPresentFullScreenContentWithError:(nonnull NSError *)error;
- (void)adDidPresentFullScreenContent:(nonnull id<GADFullScreenPresentingAd>)ad;
- (void)adDidDismissFullScreenContent:(nonnull id<GADFullScreenPresentingAd>)ad;
@end


@interface AdMobManager : NSObject

@property(nonatomic, strong) GADBannerView * bannerAd;
@property(nonatomic, strong, nullable) GADInterstitialAd * interstitialAd;
@property(nonatomic, strong, nullable) GADRewardedAd * rewardAd;


/// banner广告代理
@property (nonatomic, assign) id <AdBannerViewDelegate> adBannerDelegate;

/// 插屏、激励广告代理
@property (nonatomic, assign) id <AdFullScreenDelegate> adAdFullScreenDelegate;


/// bannerId
@property (nonatomic, copy) NSString * bannerId;
/// 设置插屏id后会加载一次插屏广告
@property (nonatomic, copy) NSString * interstitialId;
/// 设置激励id后会加载一次激励广告
@property (nonatomic, copy) NSString * rewardId;

+(nonnull instancetype) shareInstance;

/// 初始化admob
/// @param completionHandler GADInitializationCompletionHandler
+(void)startGADCompletionHandler:(nullable GADInitializationCompletionHandler)completionHandler;


/// 将bannerView移动到最上层
-(void)bringBannerViewToFront;

/// 加载Banner请求，可以不主动调用
-(void)loadBannerRequest;
/// 加载插屏请求，可以不主动调用
-(void)loadInterstitialRequestAndCompletionHandler:(nullable GADInterstitialAdLoadCompletionHandler)completionHandler;

/// 展示banner广告，superView为空时会展示在window上
/// @param origin 展示位置
-(void)showBannerViewWithOrigin:(CGPoint)origin superView:(nullable UIView *)superView;

/// 展示插屏广告
/// @param rootViewController 为空时显示在window.rootViewController上
-(void)showInterstitialWithRootViewController:(nullable UIViewController *)rootViewController;

/// 展示激励广告
/// @param rootViewController 为空时显示在window.rootViewController上
/// @param didEarnRewardHandler 观看完成，获得奖励
/// 使用[AdMobManager shareInstance].rewardAd.adReward 获取激励对象
-(void)showRewardWithRootViewController:(UIViewController *)rootViewController didEarnRewardHandler:(nullable GADUserDidEarnRewardHandler)didEarnRewardHandler;



@end

NS_ASSUME_NONNULL_END
