//
//  AdMobManager.m
//  AdMobTools
//
//  Created by ws on 2021/9/7.
//

#import "AdMobManager.h"
#import <AppTrackingTransparency/AppTrackingTransparency.h>

#define IsNilOrNull(_ref)   (((_ref) == nil) || ([(_ref) isEqual:[NSNull null]]))
#define NotNilAndNull(_ref)  (((_ref) != nil) && (![(_ref) isEqual:[NSNull null]]))

@interface AdMobManager () <GADBannerViewDelegate,GADFullScreenContentDelegate>



@end

@implementation AdMobManager

+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    static AdMobManager *manager;
    dispatch_once(&onceToken, ^{
        manager = [self new];
    });
    return manager;
}

+ (void)startGADCompletionHandler:(GADInitializationCompletionHandler)completionHandler {
    if (@available(iOS 14.0, *)) {
        [ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^(ATTrackingManagerAuthorizationStatus status) {
            
            [[GADMobileAds sharedInstance] startWithCompletionHandler:completionHandler];
        }];
    } else {
        [[GADMobileAds sharedInstance] startWithCompletionHandler:completionHandler];
    }
}


-(void)showBannerViewWithOrigin:(CGPoint)origin superView:(nullable UIView *)superView {
    
    if (IsNilOrNull(self.bannerId)) {
        NSLog(@"没有设置banner广告id");
        return;
    }
    
    GADAdSize adSize = GADCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth([UIScreen mainScreen].bounds.size.width);
    
    self.bannerAd = [[GADBannerView alloc] init];
    self.bannerAd.adSize = adSize;
    self.bannerAd.frame = CGRectMake(origin.x, origin.y, adSize.size.width, adSize.size.height);
    self.bannerAd.adUnitID = self.bannerId;
    self.bannerAd.rootViewController = [self getRootVC];
    self.bannerAd.delegate = self;

    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (superView) {
            
            if (![superView.subviews containsObject:self.bannerAd]) {
                [superView addSubview:self.bannerAd];
            }
            
            [superView bringSubviewToFront:self.bannerAd];
        } else {
            
            UIWindow * window = [self getKeyWindow];
            if (![window.subviews containsObject:self.bannerAd]) {
                [window addSubview:self.bannerAd];
            }
            
            [window bringSubviewToFront:self.bannerAd];
        }
    });
    
    
    
    
    [self loadBannerRequest];
}

-(void)showInterstitialWithRootViewController:(UIViewController *)rootViewController {
    
    UIViewController * root = NotNilAndNull(rootViewController) ? rootViewController:[self getRootVC];
    
    if (self.interstitialAd) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.interstitialAd presentFromRootViewController:root];
        });
    } else {
        NSLog(@"InterstitialAd wasn't ready");
        [self loadInterstitialRequestAndCompletionHandler:nil];
    }
}

-(void)showRewardWithRootViewController:(UIViewController *)rootViewController didEarnRewardHandler:(GADUserDidEarnRewardHandler)didEarnRewardHandler {
    
    UIViewController * root = NotNilAndNull(rootViewController) ? rootViewController:[self getRootVC];
    
    if (self.rewardAd) {
        if ([self.rewardAd canPresentFromRootViewController:rootViewController error:nil]) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.rewardAd presentFromRootViewController:root userDidEarnRewardHandler:^{
                    
//                    GADAdReward *reward = self.rewardAd.adReward;
//                    NSString *rewardMessage = [NSString
//                        stringWithFormat:@"Reward received with currency %@ , amount %lf",reward.type, [reward.amount doubleValue]];
                    NSLog(@"获得激励视频奖励");
                    didEarnRewardHandler();
                }];
            });
        } else {
            NSLog(@"RewardAd can't Present");
        }
    } else {
        NSLog(@"RewardAd wasn't ready");
        [self loadRewardRequest];
        
    }
}

-(void)bringBannerViewToFront {
    if (self.bannerAd && self.bannerAd.superview) {
        [self.bannerAd.superview bringSubviewToFront:self.bannerAd];
    }
}

#pragma mark - load ads

-(void)loadBannerRequest {
    [self.bannerAd loadRequest:[GADRequest request]];
}

-(void)loadInterstitialRequestAndCompletionHandler:( GADInterstitialAdLoadCompletionHandler)completionHandler {
    
    if (IsNilOrNull(self.interstitialId)) {
        NSLog(@"没有设置banner广告id");
        return;
    }
    
    GADRequest *request = [GADRequest request];
    [GADInterstitialAd loadWithAdUnitID:self.interstitialId
                                request:request
                      completionHandler:^(GADInterstitialAd *ad, NSError *error)
     {
        if (error) {
            NSLog(@"Failed to load interstitial ad with error: %@", [error localizedDescription]);
            return;
        }
        self.interstitialAd = ad;
        self.interstitialAd.fullScreenContentDelegate = self;
        
        if (completionHandler) {
            completionHandler(ad,error);
        }
    }];
}

-(void)loadRewardRequest {
    
    [GADRewardedAd loadWithAdUnitID:self.rewardId request:[GADRequest request] completionHandler:^(GADRewardedAd * _Nullable rewardedAd, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Failed to load Reward ad with error: %@", [error localizedDescription]);
            return;
        }
        self.rewardAd = rewardedAd;
        self.rewardAd.fullScreenContentDelegate = self;
    }];
}

#pragma mark - banner Delegate
- (void)bannerViewDidReceiveAd:(GADBannerView *)bannerView {
    NSLog(@"bannerViewDidReceiveAd");
    if([self.adBannerDelegate respondsToSelector:@selector(bannerViewDidReceiveAd:)]) {
        [self.adBannerDelegate bannerViewDidReceiveAd:bannerView];
    }
}

- (void)bannerView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(NSError *)error {
    NSLog(@"bannerView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
    if([self.adBannerDelegate respondsToSelector:@selector(didFailToPresentWithError:)]) {
        [self.adBannerDelegate bannerView:bannerView didFailToReceiveAdWithError:error];
    }
}

- (void)bannerViewDidRecordImpression:(GADBannerView *)bannerView {
    NSLog(@"bannerViewDidRecordImpression");
    if([self.adBannerDelegate respondsToSelector:@selector(bannerViewDidRecordImpression:)]) {
        [self.adBannerDelegate bannerViewDidRecordImpression:bannerView];
    }
}

- (void)bannerViewWillPresentScreen:(GADBannerView *)bannerView {
    NSLog(@"bannerViewWillPresentScreen");
    if([self.adBannerDelegate respondsToSelector:@selector(bannerViewWillPresentScreen:)]) {
        [self.adBannerDelegate bannerViewWillPresentScreen:bannerView];
    }
}

- (void)bannerViewWillDismissScreen:(GADBannerView *)bannerView {
    NSLog(@"bannerViewWillDismissScreen");
    if([self.adBannerDelegate respondsToSelector:@selector(bannerViewWillDismissScreen:)]) {
        [self.adBannerDelegate bannerViewWillDismissScreen:bannerView];
    }
}

- (void)bannerViewDidDismissScreen:(GADBannerView *)bannerView {
    NSLog(@"bannerViewDidDismissScreen");
    if([self.adBannerDelegate respondsToSelector:@selector(bannerViewDidDismissScreen:)]) {
        [self.adBannerDelegate bannerViewDidDismissScreen:bannerView];
    }
}


#pragma mark - GADFullScreenContentDelegate
/// 广告跳转失败
- (void)ad:(nonnull id<GADFullScreenPresentingAd>)ad didFailToPresentFullScreenContentWithError:(nonnull NSError *)error {
    NSLog(@"Ad did fail to present full screen content.");
    if([self.adAdFullScreenDelegate respondsToSelector:@selector(ad:didFailToPresentFullScreenContentWithError:)]) {
        [self.adAdFullScreenDelegate ad:ad didFailToPresentFullScreenContentWithError:error];
    }
}

/// 广告跳转成功
- (void)adDidPresentFullScreenContent:(nonnull id<GADFullScreenPresentingAd>)ad {
    NSLog(@"Ad did present full screen content.");
    if([self.adAdFullScreenDelegate respondsToSelector:@selector(adDidPresentFullScreenContent:)]) {
        [self.adAdFullScreenDelegate adDidPresentFullScreenContent:ad];
    }
}

/// 广告关闭
- (void)adDidDismissFullScreenContent:(nonnull id<GADFullScreenPresentingAd>)ad {
    NSLog(@"Ad did dismiss full screen content.");
    if([self.adAdFullScreenDelegate respondsToSelector:@selector(adDidDismissFullScreenContent:)]) {
        [self.adAdFullScreenDelegate adDidDismissFullScreenContent:ad];
    }
    
    [self loadInterstitialRequestAndCompletionHandler:nil];
}

#pragma mark - set
-(void)setInterstitialId:(NSString *)interstitialId {
    
    _interstitialId = interstitialId;
    
    [self loadInterstitialRequestAndCompletionHandler:nil];
}

#pragma mark - window && rootController

- (UIViewController *)getRootVC {
    
    UIViewController *result = nil;
    UIWindow * window = [self getKeyWindow];
    
    if (window.subviews.count > 0) {
        UIView *frontView = [[window subviews] objectAtIndex:0];
        id nextResponder = [frontView nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            result = nextResponder;
        } else {
            result = window.rootViewController;
        }
    } else {
        result = window.rootViewController;
    }
    
    return result;
}

- (UIWindow *) getKeyWindow {

    NSArray *windows = [[UIApplication sharedApplication] windows];
    for(UIWindow *window in [windows reverseObjectEnumerator]) {
        if ([window isKindOfClass:[UIWindow class]] &&
            window.windowLevel == UIWindowLevelNormal &&
            CGRectEqualToRect(window.bounds, [UIScreen mainScreen].bounds))
            return window;
    }
    return [UIApplication sharedApplication].keyWindow;
}

@end
