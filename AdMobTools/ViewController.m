//
//  ViewController.m
//  AdMobTools
//
//  Created by ws on 2021/9/7.
//

#import "ViewController.h"
#import "AdMobManager.h"

@interface ViewController () <AdFullScreenDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AdMobManager shareInstance].adAdFullScreenDelegate = self;
}

- (IBAction)showInterstitialHandle:(UIButton *)sender {
    
    NSLog(@"展示插屏");
    [[AdMobManager shareInstance] showInterstitialWithRootViewController:self];
}


- (IBAction)showRewardHandle:(UIButton *)sender {
    
    
    NSLog(@"展示激励");
    [[AdMobManager shareInstance] showInterstitialWithRootViewController:self];
    [[AdMobManager shareInstance] showRewardWithRootViewController:self didEarnRewardHandler:^{
        GADAdReward *reward = [AdMobManager shareInstance].rewardAd.adReward;
        NSString *rewardMessage = [NSString stringWithFormat:@"Reward received with currency %@ , amount %lf",reward.type, [reward.amount doubleValue]];
        NSLog(@"%@", rewardMessage);
    }];
}


- (void)ad:(id<GADFullScreenPresentingAd>)ad didFailToPresentFullScreenContentWithError:(NSError *)error {
    
    NSLog(@"ViewController：Fail Error");
}

- (void)adDidPresentFullScreenContent:(id<GADFullScreenPresentingAd>)ad {
    
    NSLog(@"ViewController：Present");
}

- (void)adDidDismissFullScreenContent:(id<GADFullScreenPresentingAd>)ad {
    
    NSLog(@"ViewController：Dismiss");
}


@end
