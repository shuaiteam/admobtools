//
//  AppDelegate.m
//  AdMobTools
//
//  Created by ws on 2021/9/7.
//

#import "AppDelegate.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "AdMobManager.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    if (@available(iOS 13.0, *)) {
        return YES;
    }
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window makeKeyAndVisible];
    self.window.rootViewController = [UIViewController new];

    
    
    [AdMobManager startGADCompletionHandler:nil];
    
    [AdMobManager shareInstance].bannerId = @"ca-app-pub-3940256099942544/2934735716";
    [AdMobManager shareInstance].interstitialId = @"ca-app-pub-3940256099942544/4411468910";
    [AdMobManager shareInstance].rewardId = @"ca-app-pub-3940256099942544/1712485313";
    
    NSLog(@"加载banner");
    
    [[AdMobManager shareInstance] showBannerViewWithOrigin:CGPointMake(100, 100) superView:self.window];

    
    
    return YES;
}


#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}


@end
